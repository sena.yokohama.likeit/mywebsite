package beans;

public class BuyData {
	private int id;
	private int userId;
	private int totalPrice;
	private String deliveryname;
	private String createDate;

	private String deliveryMethodName;
	private int deliveryMethodPrice;
	private int formatTotalPrice;



	public BuyData(int id, int userId, int totalPrice, String deliveryname, String createDate) {
		this.id = id;
		this.userId = userId;
		this.totalPrice = totalPrice;
		this.deliveryname = deliveryname;
		this.createDate = createDate;
	}

	public BuyData(int id, int totalPrice, String createDate, String deliveryMethodName, int userId,
			int deliveryMethodPrice, String deliveryname) {
		this.id = id;
		this.totalPrice = totalPrice;
		this.createDate = createDate;
		this.deliveryMethodName = deliveryMethodName;
		this.userId = userId;
		this.deliveryMethodPrice = deliveryMethodPrice;
		this.deliveryname = deliveryname;

	}



	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}

	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}

	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}

	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}

	public int getFormatTotalPrice() {
		return formatTotalPrice;
	}

	public void setFormatTotalPrice(int formatTotalPrice) {
		this.formatTotalPrice = formatTotalPrice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getDeliveryname() {
		return deliveryname;
	}

	public void setDeliveryname(String deliveryname) {
		this.deliveryname = deliveryname;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

}
