package beans;

public class ItemBeans {

	private int id;
	private String name;
	private String detail;
	private int price;
	private String filename;

	public ItemBeans(int id, String name, String detail, int price) {
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.price = price;
	}

	public ItemBeans(int id, String name, String detail, int price,String filename) {
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.filename = filename;
	}

	public ItemBeans(String name, String detail) {
		this.name = name;
		this.detail = detail;

	}

	public ItemBeans(String name) {
		this.name = name;

	}

	public ItemBeans(String name, String detail, int price) {
		this.name = name;
		this.detail = detail;
		this.price = price;
	}

	public ItemBeans(String name, int price) {
		this.name = name;
		this.price = price;
	}


	public ItemBeans() {
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDetail() {
		return detail;
	}


	public void setDetail(String detail) {
		this.detail = detail;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public String getFilename() {
		return filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}



}
