package beans;

public class User {
	private int id;
	private String name;
	private String loginId;
	private String password;
	private String birthDate;
	private String createDate;

	// ログインセッションを保存するためのコンストラクタ
	public User(String loginId,int id, String name) {
		this.loginId = loginId;
		this.id = id;
		this.name = name;
	}

	// 全てのデータをセットするコンストラクタ
	public User(int id, String name, String loginId, String password, String birthDate, String createDate) {
		this.id = id;
		this.name = name;
		this.loginId = loginId;
		this.password = password;
		this.birthDate = birthDate;
		this.createDate = createDate;
	}

	public User(int id, String name, String loginId, String birthDate, String createDate) {
		this.id = id;
		this.name = name;
		this.loginId = loginId;
		this.birthDate = birthDate;
		this.createDate = createDate;

	}

	public User(String name) {
		this.name = name;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

}
