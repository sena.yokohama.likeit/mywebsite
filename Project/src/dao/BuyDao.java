package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.BuyData;

public class BuyDao {
	//購入登録
	public int BuyData(String userid, int price, int buydelivery) {
		Connection conn = null;
		int autoIncKey = 0;
		// ここに内部の処理を書いていきます
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			String sql = "INSERT INTO buy(user_id, buy_total_price, buy_delivery_name, create_date) values (?,?,?,now())";
			PreparedStatement pStmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pStmt.setString(1, userid);
			pStmt.setInt(2, price);
			pStmt.setInt(3, buydelivery);

			pStmt.executeUpdate();
			ResultSet rs = pStmt.getGeneratedKeys();

			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}

			return autoIncKey;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return autoIncKey;
	}

	//履歴
	public List<BuyData> bdList(String id) {

		Connection conn = null;
		// ここに内部の処理を書いていきます
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			List<BuyData> buyList = new ArrayList<BuyData>();
			// SELECT文を準備
			String sql = "SELECT * FROM  buy INNER JOIN delivery ON " +
					"buy.buy_delivery_name = delivery.id " +
					"WHERE user_id=? ORDER BY create_date ASC;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int idp = rs.getInt("buy.id");
				int userid = rs.getInt("user_id");
				int totalprice = rs.getInt("buy_total_price");
				String deliveryname = rs.getString("name");
				Date createdate = rs.getDate("create_date");

				//data型をstring型にする
				String str = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(createdate);
				BuyData buy = new BuyData(idp, userid, totalprice, deliveryname, str);
				buyList.add(buy);
			}
			return buyList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//購入詳細
	public int BuyDetail(int buyid, int id) {

		Connection conn = null;
		// ここに内部の処理を書いていきます
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			String sql = "INSERT INTO buy_detail(buy_id,item_id) VALUES(?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, buyid);
			pStmt.setInt(2, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return buyid;

	}

	public BuyData bd(int id) {
		Connection conn = null;
		// ここに内部の処理を書いていきます
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM buy JOIN delivery  ON buy.buy_delivery_name = delivery.id " +
					" WHERE buy.id = ?;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id2 = rs.getInt("id");
			int totalprice = rs.getInt("buy_total_price");
			String createdate = rs.getString("create_date");
			String deliverymethoname = rs.getString("name");
			int userid = rs.getInt("user_id");
			int deliveryprice = rs.getInt("price");
			String deliveryname = rs.getString("name");

			BuyData BuyData = new BuyData(id2, totalprice, createdate, deliverymethoname, userid, deliveryprice,
					deliveryname);

			return BuyData;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}
}
