package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.ItemBeans;

public class ItemDao {
	private static final ArrayList<ItemBeans> ArrayList = null;

	//アイテムデータ
	public ItemBeans itemdata(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM  item WHERE item_id=?;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int idi = rs.getInt("item_id");
			String name = rs.getString("item_name");
			String detail = rs.getString("item_detail");
			int price = rs.getInt("item_price");
			String filename = rs.getString("file_name");

			ItemBeans item = new ItemBeans(idi, name, detail, price, filename);

			return item;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//ランダムアイテムデータ
	public static ArrayList<ItemBeans> itemdataList(int limit) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM  item ORDER BY RAND() limit ?;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, limit);
			ResultSet rs = pStmt.executeQuery();

			ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

			while (rs.next()) {
				ItemBeans item = new ItemBeans();
				item.setId(rs.getInt("item_id"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFilename(rs.getString("file_name"));
				itemList.add(item);
			}

			return itemList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//アイテム削除
	public void delete(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "DELETE FROM item WHERE item_id = ?;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//検索
	public List<ItemBeans> search(String name) {
		Connection conn = null;
		List<ItemBeans> itemList = new ArrayList<ItemBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			//部分一致
			String sql = "SELECT * FROM item WHERE item_detail LIKE '" + '%' + name + '%' + "'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 取得したレコード数分繰り返す
			while (rs.next()) {
				int itemid = rs.getInt("item_id");
				String itemname = rs.getString("item_name");
				String itemdetail = rs.getString("item_detail");
				int itemprice = rs.getInt("item_price");
				String filename = rs.getString("file_name");

				ItemBeans searchitem = new ItemBeans(itemid, itemname, itemdetail, itemprice, filename);
				// リストに追加
				itemList.add(searchitem);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return itemList;
	}

	//詳細
	public ItemBeans itemdb(int buyId) {
		Connection conn = null;
		// ここに内部の処理を書いていきます
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM buy_detail JOIN item ON buy_detail.item_id = item.item_id  WHERE buy_detail.buy_id = ?;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, buyId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id2 = rs.getInt("id");
			String itemname = rs.getString("item_name");
			String itemdetail = rs.getString("item_detail");
			int itemprice = rs.getInt("item_price");

			ItemBeans Itemdb = new ItemBeans(id2, itemname, itemdetail, itemprice);

			return Itemdb;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//アイテムデータ
	public ArrayList<ItemBeans> itemdatalist(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM  item WHERE item_id=?;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

			if (!rs.next()) {
				return null;
			}

			while (rs.next()) {
				ItemBeans item2 = new ItemBeans();
				item2.setId(rs.getInt("item_id"));
				item2.setName(rs.getString("item_name"));
				item2.setDetail(rs.getString("item_detail"));
				item2.setPrice(rs.getInt("item_price"));
				item2.setFilename(rs.getString("file_name"));
				itemList.add(item2);
			}

			return itemList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//お気に入り一覧
	public ArrayList<ItemBeans> favoriteList(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

			// SELECT文を準備
			String sql = "SELECT * FROM  `favorite` INNER JOIN item ON favorite.item_id = item.item_id WHERE user_id=? ;";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				int userid = rs.getInt("user_id");
				String itemid = rs.getString("item_id");
				String itemname = rs.getString("item_name");
				int price = rs.getInt("item_price");

				ItemBeans favoriteitem = new ItemBeans(userid, itemid, itemname, price);
				itemList.add(favoriteitem);
			}

			return itemList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	//お気に入り登録
	public int favorite(String userid, int itemid) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			String sql = "INSERT INTO `favorite` (`user_id`, `item_id`) VALUES (?, ?);";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userid);
			pStmt.setInt(2, itemid);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return itemid;
	}
}