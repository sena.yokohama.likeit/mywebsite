package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Delivery;
import beans.ItemBeans;
import dao.DeliveryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Cart3
 */
//注文確認
@WebServlet("/Cart3")
public class Cart3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Cart3() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		// 文字化け防止
		request.setCharacterEncoding("UTF-8");

		//item idを受け取る
		String id = request.getParameter("id");

		//Dao
		ItemDao ItemDao = new ItemDao();
		ItemBeans item = ItemDao.itemdata(id);

		request.setAttribute("itemdata", item);

		//配送方法IDを取得
		String id2 = request.getParameter("delivery");
		//DeliveryDao
		DeliveryDao DeliveryDao = new DeliveryDao();
		Delivery db = DeliveryDao.Deliverydata(id2);

		//アイテムプライスと配送料を足す
		int totalprice = item.getPrice() + db.getPrice();

		request.setAttribute("Delivery", db);
		request.setAttribute("totalprice", totalprice);



		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart3.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
