package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class NewUser
 */
@WebServlet("/NewUser")
public class NewUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String birthDate = request.getParameter("birthDate");

		//Dao
		UserDao userDao = new UserDao();

		//passwordが一致しなかったら
		if (!password.equals(password1)) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードが一致しません");
			// ログイン画面に戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//未入力がある場合
		if (loginId.equals("") || password.equals("") || password1.equals("") || name.equals("")
				|| birthDate.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "未入力欄があります");
			// ログイン画面に戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//登録済みのIDがある場合
		User user = UserDao.id(loginId);
		if (user != null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "登録済みのIDです");
			// ログイン画面に戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//暗号化
		String pass = userDao.pass(password);
		userDao.newuser(name, loginId, pass, birthDate);

		//登録完了画面へ
		response.sendRedirect("NewUser2");

	}

}
