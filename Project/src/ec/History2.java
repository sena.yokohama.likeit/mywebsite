package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BuyData;
import beans.ItemBeans;
import dao.BuyDao;
import dao.ItemDao;

/**
 * Servlet implementation class History2
 */
@WebServlet("/History2")
public class History2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public History2() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 文字化け防止
		request.setCharacterEncoding("UTF-8");

		//buyidを受け取る
		int id = Integer.parseInt(request.getParameter("id"));

		//item list
		ItemDao ItemDao = new ItemDao();
		ItemBeans idb = ItemDao.itemdb(id);

		//Daoを作る
		BuyDao buyDao = new BuyDao();
		BuyData bd = buyDao.bd(id);

		request.setAttribute("idb", idb);
		request.setAttribute("bd", bd);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/history2.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
