package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Delivery;
import beans.ItemBeans;
import dao.BuyDao;
import dao.DeliveryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Cart4
 */
@WebServlet("/Cart4")
public class Cart4 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Cart4() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 文字化け防止
		request.setCharacterEncoding("UTF-8");

		//user idを受け取る
		String userid = request.getParameter("userid");

		//item idを受け取る
		String id = request.getParameter("id");


		//Dao
		ItemDao ItemDao = new ItemDao();
		ItemBeans item = ItemDao.itemdata(id);

		//配送方法IDを取得
		String id2 = request.getParameter("delivery");

		//DeliveryDao
		DeliveryDao DeliveryDao = new DeliveryDao();
		Delivery db = DeliveryDao.Deliverydata(id2);

		//アイテムプライスと配送料を足す
		int totalprice = item.getPrice() + db.getPrice();

		request.setAttribute("Delivery", db);

		BuyDao bdao = new BuyDao();
		int buyid = bdao.BuyData(userid, totalprice, Integer.parseInt(id2));

		request.setAttribute("bdao", bdao);

		//購入詳細に登録
		BuyDao buydao = new BuyDao();
		int bdbd = buydao.BuyDetail(buyid,item.getId());

		request.setAttribute("bdbd", bdbd);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart4.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
