
package ec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemBeans;
import beans.User;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class Top
 */
@WebServlet("/Top")
public class Top extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Top() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 文字化け防止
		request.setCharacterEncoding("UTF-8");

		//ArrayList
		ArrayList<ItemBeans> itemList = ItemDao.itemdataList(6);
		request.setAttribute("itemList", itemList);

		//user idを受け取る
		String id = request.getParameter("id");

		//Daoを作る
		UserDao userDao = new UserDao();
		User user = userDao.UserData(id);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/top.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 文字化け防止
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String search = request.getParameter("search");

		//Dao
		ItemDao ItemDao = new ItemDao();
		List<ItemBeans> searchlist = ItemDao.search(search);

		//userlistにセット
		request.setAttribute("searchlist", searchlist);

		//userlistに表示
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/top.jsp");
		dispatcher.forward(request, response);
	}

}
