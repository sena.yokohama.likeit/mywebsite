<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>検索結果</title>
<link href="css/search.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="aaa">EC sample</div>

	<hr width="1400px" color=#333333 size="0.5px">

	<br>
	<br>
	<h3>検索結果</h3>

	<form action="Search">
		<div class="row">
			<div class="card">

				<c:forEach var="search" items="${searchList}">
					<div class="card__imgframe1">
						<a href="Item?id=${search.id}"> <img alt=""
							src="img/${search.filename}" width="350px"></a>
					</div>
					<div class="card__textbox">
						<div class="card__titletext">${search.name}</div>
						<div class="card__overviewtext">￥${search.price}</div>
					</div>

				</c:forEach>
			</div>
		</div>

		<a href="Top" class="bt2">戻る</a>
	</form>
</body>


</html>