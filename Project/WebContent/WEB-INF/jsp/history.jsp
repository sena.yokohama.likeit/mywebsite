<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>注文履歴</title>
<link href="css/History.css" rel="stylesheet" type="text/css">
</head>
<body>
	<form action="History" method="post">
		<h1>${userdata.name}様注文履歴</h1>
		<hr wibtth="1500px">
		<h4>注文履歴</h4>
		<c:forEach var="bdb" items="${bdbList}">
			<th>購入日時 / ${bdb.createDate}</th>

			<br>

			<th>配送方法 / ${bdb.deliveryname}</th>


			<br>

			<th>購入金額 / ¥ ${bdb.totalPrice}</th>
			<br>
			<br>
			<br>
			<div class=bt>
				<a href="History2?id=${bdb.id}" class="btn">注文詳細</a>
			</div>
		</c:forEach>
		<br> <br> <br>
		<div class=back>
			<a href="Top" class="bt2">戻る</a>
		</div>
	</form>
</body>


</html>
