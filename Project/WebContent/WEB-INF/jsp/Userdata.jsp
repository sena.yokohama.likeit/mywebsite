<%@page import="beans.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員情報</title>
<link href="css/Userdata.css" rel="stylesheet" type="text/css">
</head>
<body>
	<form action="UserData">
		<h3>${userdata.name}様の会員登録情報</h3>
		<div class=hako2>
			<h4>会員登録情報</h4>
			<hr wibtth="1500px">
			<br>
			<div class="form-group row">
				<label for="exampleInputEmail1">ログインID ${userdata.loginId}</label>
			</div>
			<br>
			<br>
			<div class="form-group">
				<label for="exampleInputPassword1">ユーザー名 ${userdata.name} </label>
			</div>
			<br>
			<br>
			<div class="form-group">
				<label for="exampleInputPassword1">生年月日 ${user.birthDate}</label>
			</div>
			<br>
			<br>
			<div class="form-group">
				<label class="input-group-text">登録日時 ${user.createDate}</label>
			</div>

			<br>
			<br>
			<div class=button>
				<td><a href="History?id=${userdata.id}" class="bt">注文履歴</a></td>
			</div>


			<div class=button>
				<td><a href="Updata?id=${userdata.id}" class="bt">更新</a></td>
			</div>

			<div class=back>
				<a href="Top" class="bt">戻る</a>
			</div>


		</div>
	</form>
</body>


</html>
