<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録</title>
<link href="css/new.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class=hako>

		<form action="NewUser" method="post">
			<h1>新規登録</h1>

			<div class="form-group">
				<label class="input-group-text">ユーザー名 </label> <br> <input
					type="text" class="form-group" name="name">
			</div>
			<br> <br>
			<div class="form-group row">
				<label for="exampleInputEmail1">ログインID</label> <br> <input
					type="text" class="form-group" id="exampleInputEmail1"
					aria-describedby="emailHelp" name="loginId">
			</div>
			<br> <br>
			<div class="form-group">
				<label for="exampleInputPassword1">パスワード</label> <br> <input
					type="password" class="form-group" id="exampleInputPassword1"
					name="password">
			</div>
			<br> <br>
			<div class="form-group">
				<label for="exampleInputPassword1">パスワード(確認)</label> <br> <input
					type="password" class="form-group" id="exampleInputPassword1"
					name="password1">
			</div>

			<br> <br>

			<div class="form-group">
				<label>生年月日</label> <br> <input type="date" class="form-group"
					name="birthDate">
			</div>
			<br>

			<h3 style="text-align: center">
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
			</h3>

			<div class="btn">
				<button type="submit">登録</button>
			</div>

		</form>
		<a href="Login" class=back>戻る</a>




	</div>

</body>
</html>