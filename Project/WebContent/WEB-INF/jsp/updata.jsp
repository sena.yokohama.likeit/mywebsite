<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員情報更新</title>
<link href="css/updata.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h4>EC sample</h4>
	<h3>${userdata.name}様の会員登録情報</h3>
	<div class=hako2>

		<hr wibtth="1500px">

		<form action="Updata" method="post">

			<h5>会員登録情報更新</h5>

			<div class="form-group">
				<label>ログインID ${userdata.id}</label> <input type="hidden" name="id"
					value="${userdata.id}" class="form-control"> <br> <br>

				<label>パスワード</label> <br> <input type="password" class="form"
					id="exampleInputPassword1" name="password"> <br> <br>

				<label>パスワード(確認)</label> <br> <input type="password"
					class="form" id="exampleInputPassword1" name="password1"> <br>
				<br> <label>ユーザー名 </label> <br> <input type="text"
					aria-label="First name" class="form" name="name"
					value="${userdata.name}"> <br>
				<br> <label>生年月日</label> <br> <input type="date"
					class="form" name="birthDate" value="${userdata.birthDate}">



			</div>
			<h2 style="text-align: center">
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
			</h2>


			<br> <br>
			<div class="btn">
				<button type="submit" class="form-group">更新</button>
			</div>
			<br> <br> <a href="UserData" class=back>戻る</a>
		</form>
	</div>

</body>


</html>
