<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link href="css/login.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="hako">
		<h1>Login</h1>

		<form action="Login" method="post">


			<div class="form-group">

				<input type="text" name="loginId" id="inputLoginId"
					class="form-group" placeholder="ID"> <br> <br> <input
					type="password" name="password" id="inputPassword"
					class="form-group" placeholder="password" required> <br>
				<br>
				<div class="form-item" >
					<p class="pull-left" >
						<a href="NewUser" style="color:black"><small >新規登録</small></a>
					</p>
				</div>
				<br> <br>
			<h3 style="text-align: center">
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
			</h3>

				<div>
					<button type="submit" class="btn">Login</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>
