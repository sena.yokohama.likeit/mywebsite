<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お気に入り一覧</title>
<link href="css/favorite2.css" rel="stylesheet" type="text/css">
</head>
<body>

	<h3>EC sample</h3>


	<br>
	<hr width="1400px" color=#333333 size="0.5px">



	<h4>お気に入り一覧</h4>

	<c:forEach var="favorite" items="${favoriteList}">
		<p>商品名/${favorite.detail}</p>
		<p>金額 /¥ ${favorite.price}</p>

		<br>

	</c:forEach>
	<br>
	<br>
	<div class=back>
		<a href="Top" class="bt2">戻る</a>
	</div>
</body>
</html>