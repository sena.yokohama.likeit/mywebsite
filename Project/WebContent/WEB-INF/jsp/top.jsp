<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>トップ</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<link href="css/top.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="aaa">EC sample</div>
	<h7>${userdata.name} 様</h7>





	<div class="bt">

		<a href="UserData?id=${userdata.id}"> <img
			src="img/%E3%82%B3%E3%83%9F%E3%83%A5%E3%83%8B%E3%82%B1%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3%E3%82%A2%E3%82%A4%E3%82%B3%E3%83%B33.jpeg"
			class="btn2">
		</a> <a href="Favorite2?id=${userdata.id}"> <img
			src="img/%E3%83%8F%E3%83%BC%E3%83%88%E3%81%AE%E3%83%9E%E3%83%BC%E3%82%AF.jpeg"
			class="btn2">
		</a> <a href="Logout"> <img
			src="img/%E3%83%AD%E3%82%B0%E3%82%A4%E3%83%B3%E3%83%89%E3%82%A2%E3%80%80%E3%82%A2%E3%82%A4%E3%82%B3%E3%83%B32.jpeg"
			class="btn2">
		</a>
	</div>
	<br>


	<hr width="1400px" color=#333333 size="0.5px">

	<form action="Top" method="post">
		<div class="form-group">
			<input type="text" class="form-group" name="search">
		</div>


		<div class="row">
			<c:forEach var="item" items="${itemList}">
				<div class="card">


					<div class="card__imgframe1">

						<a href="Item?id=${item.id}"><img src="img/${item.filename}"
							width="350px" height="400px"></a>

					</div>
					<div class="card__textbox">
						<div class="card__titletext">${item.name}</div>
						<div class="card__overviewtext">￥${item.price}</div>
					</div>


				</div>
			</c:forEach>
		</div>



		<div class="row">
			<c:forEach var="searchitem" items="${searchlist}">
				<div class="card">
					<div class="card__imgframe1">

						<a href="Item?id=${searchitem.id}"><img
							src="img/${searchitem.filename}" width="350px"></a>

					</div>
					<div class="card__textbox">
						<div class="card__titletext">${searchitem.name}</div>
						<div class="card__overviewtext">￥${searchitem.price}</div>
					</div>
				</div>
			</c:forEach>

		</div>

	</form>

</body>


</html>