/*-------------------------------------
SITE : http: //zozo.jp/
CREDIT : DESIGN BY STARTTODAY CO.,LTD.
CREATE : 2015.11.11
MODIFIED :  2015.04.15
Var : 1.0
MEMO: 
-------------------------------------*/


({
	conf:{
	},

	/*-------------------------------------
		初期化
	-------------------------------------*/
	init:function(){
		var _this=this;
		var conf=this.conf;

		$(function(){
			_this.smpleOverray.init();
		});
	},

	/*-------------------------------------
		サンプルオーバーレイ
	--------------------------------------*/
	smpleOverray:{
		conf:{
			overlayHeight:null
		},

		init:function(){
		var _this=this;
			$(function(){
				// overray
				$("#goodMainArea").on("click",".sampleLink", function(){
					if ($("#sampleInfo").css("display") == 'none' ) {
						$("#sampleInfo").css("top",$(document).scrollTop());
						if ($("#sampleInfoInner").find("ul").length == 0) {
							_this.openGiftCardSample();
						}
						if($.ua.isIE7){}else{
							$("#clearBg_smp").removeClass('hide');
						}
						$("#sampleInfo").fadeIn();
					}
					return false;
				});
			});
		},

		openGiftCardSample:function(){
			var _this=this;
			var _conf=this.conf;
			var id='#giftSampleOverray';
			var $wrapper=$(id);
			var imgURL = 'http://img4.zozo.jp/'
			if($wrapper.find('.popWin').length==0){
			var elm=[
					'		<div id="sampleInfoInner">',
					'			<p id="sampleInfoClose"><img src="http://zozo.jp/common_2013/img/ico/ico_cancel.png"></p>',
					'			<div id="sampleInfoTitle">',
					'				<p class="title">写真入りメッセージカードで<br>思い出に残るギフトを届けよう</p>',
					'				<p class="lead">忙しくて会えないご友人に、孫の成長を楽しみにしているおじいちゃん、おばあちゃんに、<br>大好きなご家族や恋人に、サプライズなメッセージを贈りませんか？</p>',
					'			</div>',
					'			<div id="SampleList" class="loading"></div>',
					'			<p id="next" class="ctrl"></p>',
					'			<p id="prev" class="ctrl"></p>',
					'		</div>',
					'		<div id="dot"></div>'
				].join('');
			$wrapper.find('#sampleInfo').append(elm);
		}

			//閉じイベント
			$("#giftSampleOverray").on("click","#sampleInfoClose",function() {
				if ($("#sampleInfo").css("display") == 'block' ) {
					$("#clearBg_smp").addClass('hide');
					$("#sampleInfo").fadeOut();
				}
			});
			$("#clearBg_smp").click(function() {
				if ($("#sampleInfo").css("display") == 'block' ) {
					$("#clearBg_smp").addClass('hide');
					$("#sampleInfo").fadeOut();
				}
			});
			//画像表示
			_this.rollingImg.init();
		},

		/*-------------------------------------
		サンプル画像横ズリ
		--------------------------------------*/
		rollingImg:{
			brandArr:[
				{
					name:'1',
					img:'Sample01.jpg'
				},
				{
					name:'2',
					img:'Sample02.jpg'
				},
				{
					name:'3',
					img:'Sample03.jpg'
				},
				{
					name:'4',
					img:'Sample04.jpg'
				},
				{
					name:'5',
					img:'Sample05.jpg'
				},
				{
					name:'6',
					img:'Sample06.jpg'
				},
				{
					name:'7',
					img:'Sample07.jpg'
				},
				{
					name:'8',
					img:'Sample08.jpg'
				},
				{
					name:'9',
					img:'Sample09.jpg'
				}
			],
			cnt:1,
			elms:[],
			init:function(){
				var _this=this;
				$(function(){
					_this.createArr();
					_this.createDot();
					_this.addEvent();
				});
			},

			// 要素生成
			createArr:function(){
				var _this=this;
				var arr=_this.brandArr;
				var newArr=[];

				//  ランダムに並び替え
				// for(var i=0,len=_this.brandArr.length;i<len;i++){
				// 	var max=arr.length;
				// 	var n=Math.floor(Math.random() * max--);
				// 	newArr.push(arr[n]);
				// 	arr.splice(n,1);
				// }
				// _this.brandArr=newArr;

				$.each(_this.brandArr,function(){
					var elm='<li><img src="http://img4.zozo.jp/giftwrapping/pc/'+this.img+'" width="380" height="560" alt="'+this.name+'" /></li>';
					_this.elms.push(elm);
				});

				var str='<ul class="clearfix">';
				for(var i=0,len=_this.elms.length;i<len&&i<5;i++){
					str+=_this.elms[i];
				}
				str+='</ul>';

				$('#SampleList').append(str);
				$('#SampleList').find('ul').css({marginLeft:-667});
			},

			// ドットナビ
			createDot:function(){
				var _this=this;
				var str='<ul class="clearfix">';
				for(var i=0;i<_this.brandArr.length;i++){
					if(i==0){
						str+='<li class="current"><span></span></li>';
					}else{
						str+='<li><span></span></li>';
					}
				}
				str+='</ul>';
				$('#dot').append(str);
			},

			// 画像切り替え
			changeImg:function(flg){
				var _this=this;
				var brandArrLen=this.brandArr.length;
				var $ul=$('#sampleInfoInner').find('ul');

				if(flg=='next'){
					var nextItem = (_this.cnt+3)%brandArrLen;
					$ul.stop().animate({marginLeft:-1097+'px'},900,'easeOutExpo',function(){
						$ul.find('li').eq(0).remove().end().end().append(_this.elms[nextItem]).css({marginLeft:-667});
					});
				}else{
					var prevItem = (_this.cnt+brandArrLen-1)%(brandArrLen);
					$ul.stop().animate({marginLeft:-237+'px'},900,'easeOutExpo',function(){
						$ul.find('li').filter(':last').remove().end().end().prepend(_this.elms[prevItem]).css({marginLeft:-667});
					});
				}

				$('#dot').find('li').removeClass('current').eq(_this.cnt-1).addClass('current');
			},

			// クリックイベント設定
			addEvent:function(){
				var _this=this;
				var brandArrLen = this.brandArr.length;

				var $sampleInfo=$('#sampleInfo');

				$('#giftSampleOverray').on("click","#next",function(){
					if($sampleInfo.find(':animated').length==0){
						_this.cnt=(_this.cnt+1)%(brandArrLen);
						_this.changeImg('next');

					}
				});
				$('#giftSampleOverray').on("click","#prev",function(){
					if($sampleInfo.find(':animated').length==0){
						_this.cnt=(_this.cnt+brandArrLen-1)%(brandArrLen);
						_this.changeImg('prev');
					}
				});
			}
		}
	}

}).init();

