var __funcLaterPay=(function($, window, document, undefined) {
	var __$win;
	var __$doc;
	var __$html;
	var __$body;
	var __$modalTpl;
	var __$trigger;
	var __$overlay;
	var __$laterContent;
	var __defaultPos=0;
	
	function __init(){
		$(function(){
			__$win = $(window);
			__$doc = $(document);
			__$html = $('html');
			__$body = $('body');

			__$trigger=$('[data-toggle="later_pay"]');

			if(__$trigger.length==0){
				return;
			}

			__$trigger.one('click',function(e){
				__createModal();
				e.preventDefault();
			}).on('click',function(e){
				__openModal();
				e.preventDefault();
			});
		});
	}

	function __createModal(){
		__$modalTpl=$(__modalTpl).appendTo(__$body);
		__$overlay=__$modalTpl.find('.overlay');
		__$laterContent=__$modalTpl.find('.laterContent');
		__$overlay.css({opacity:0}).on('click',function(){
			__closeModal();
		});
		__$modalTpl.find('[data-dismiss="later_pay"]').on('click',function(){
			__closeModal();
		});


		__$laterContent.css({opacity:0});
	}

	function __openModal(){
		__defaultPos=__$win.scrollTop();
		var winScroll=__defaultPos+200;
		__$body.addClass('modalOpen');
		__$overlay.animate({opacity:0.7},120,function(){
			__$laterContent.css({opacity:0,top: winScroll}).animate({
				opacity:1,
				top: '-=100'
			},300);
		});
	}

	function __closeModal(){
		__$laterContent.animate({
			opacity:0,
			top: '+=100'
		},300,function(){
			__$win.scrollTop(__defaultPos);
			__$overlay.animate({opacity:0},120,function(){
				__$body.removeClass('modalOpen');
			});
		});
	}


	var __modalTpl=[
		'<div id="laterModal">',
		'	<div class="overlay"></div>',
		'	<div class="laterContent">',
		'		<div>',
		'			<h2 class="cover"><img src="' + window.__app.storageOriginalUrl + 'later-payment/pc/lp/main_img_v2.png" alt="ツケ払いならお支払いは2ヶ月後"></h2>',
		'		</div>',
		'		<div class="later-payment-read">',
		'			ZOZOTOWNのツケ払いなら<br>',
		'			お支払いは2ヶ月後でOK!<br>',
		'			<span class="textImportant">最大5万円までご利用いただけます</span>',
		'		</div>',
		'		<section id="laterSec01" class="modalSection">',
		'			<h3 class="laterSectionH">初めての方でも安心</h3>',
		'			<div class="laterSectionBody">',
		'				<p class="laterTxt">',
		'					ツケ払いなら商品を受け取り後に<br>',
		'					お支払いができるので、<br>',
		'					安心してご利用いただけます。',
		'				</p>',
		'				<p class="laterInfo">',
		'					※予約商品、ゲスト購入、ギフトラッピングサービスでのご利用はできません。<br>',
		'					※手数料' + window.laterPaymentInfo.fee + '円（税込）を別途いただいております。<br>',
		'					※請求書は商品と同梱してお届けいたします。',
		'				</p>',
		'			</div>',
		'		</section>',
		'		<section id="laterSec02" class="modalSection">',
		'			<h3 class="laterSectionH">利用限度額 <span class="textImportant font-bold">最大<span class="later-payment-limit">5</span>万円</span></h3>',
		'			<div class="laterSectionBody">',
		'				<p class="laterTxt">',
		'					大きなお買い物にもご利用いただける<br>',
		'					ご利用枠を用意しています。',
		'				</p>',
		'				<p class="laterInfo">',
		'					※' + window.laterPaymentInfo.maximumAmount + '円（税込）以上のご注文でのご利用はできません。<br>',
		'				</p>',
		'			</div>',
		'		</section>',
		'		<section id="laterSec025" class="modalSection">',
		'			<h3 class="laterSectionH">便利なお支払い方法</h3>',
		'			<div class="laterSectionBody">',
		'				<p class="laterTxt">',
		'					コンビニ・銀行・LINE Payのいずれかでお支払いいただけます。<br>',
		'					お支払い期間は商品購入から2ヶ月間あり、<br>',
		'					お客様のご都合のよいタイミングでの<br>',
		'					お支払いが可能です。',
		'				</p>',
		'				<p class="laterInfo">',
		'					※ツケ払いによる代金の請求、<br>',
		'					並びに請求に関してご連絡が必要となる場合、<br>',
		'					GMOペイメントサービスがおこないます。',
		'				</p>',
		'			</div>',
		'		</section>',
		'<section id="laterSec03" class="modalSection">',
			'<h3 class="laterSectionH">ツケ払いの流れ</h3>',
			'<div class="laterSectionBody">',
				'<div class="later-payment-flow">',
					'<ol class="later-payment-flow-list">',
						'<li class="later-payment-flow-item">',
							'<div class="later-payment-flow-item-thumbs"><img src="' + window.__app.storageOriginalUrl + 'later-payment/sp/info_img_01.gif" alt=""></div>',
							'<div class="later-payment-flow-item-contents">',
								'<p class="later-payment-flow-item-heading">1.商品購入</p>',
								'<p class="later-payment-flow-item-description">',
									'商品のお支払い方法で<br>',
									'「<span class="att">ツケ払い</span>」をお選びください。',
								'</p>',
							'</div>',
						'</li>',
						'<li class="later-payment-flow-item">',
							'<div class="later-payment-flow-item-thumbs"><img src="' + window.__app.storageOriginalUrl + 'later-payment/sp/info_img_02.gif" alt=""></div>',
							'<div class="later-payment-flow-item-contents">',
								'<p class="later-payment-flow-item-heading">2.商品発送・請求書</p>',
								'<p class="later-payment-flow-item-description">',
									'購入完了後、<span class="att">請求書は商品と同梱</span>して<br>',
									'お届けいたします。',
								'</p>',
							'</div>',
						'</li>',
						'<li class="later-payment-flow-item">',
							'<div class="later-payment-flow-item-thumbs"><img src="' + window.__app.storageOriginalUrl + 'later-payment/sp/info_img_04.gif" alt=""></div>',
							'<div class="later-payment-flow-item-contents">',
								'<p class="later-payment-flow-item-heading">3.お支払い</p>',
								'<p class="later-payment-flow-item-description">',
									'<span class="att">請求書記載の期日内</span>に<br>',
									'コンビニ・銀行・LINE Payのいずれかで<br>',
									'お支払いください。',
								'</p>',
							'</div>',
						'</li>',
					'</ol>',
					'<p class="later-payment-flow-note">※ツケ払いによる代金の請求、並びに請求に関してご連絡が必要となる場合、GMOペイメントサービスがおこないます。</p>',
				'</div>',
			'</div>',
		'</section>',
		'		<div class="laterBtnArea clearfix">',
		'			<a class="close gBtnWhite btnM" data-dismiss="later_pay" href="#">閉じる</a>',
		'		</div>',
		'		<a class="closeBtn" data-dismiss="later_pay" href="#"><img src="/common_2013/img/ico/ico_cancel.png" alt="close" title="close"></a>',
		'	<!-- /.content --></div>',
		'</div>',
	].join("\n");


	__init();


		return {init:__init};

	
})(jQuery, window, document);
