(function($, window, document, undefined) {
	var __$win = $(window);
	var __$doc = $(document);
	var __$html = $('html');
	var __$body = $('body');
	var __app = window.__app = window.__app || {};

	var __coupon=window.coupon||{ExclusionShop:''};
	
	function __init(){
		$(function(){
			__addHeaderLabel();
			__showHeaderTime();
			__showCouponModal();
		});
	}

	/**
	 * ------------------------------------------------------------
	 * ヘッダのボタンにDataLayer設定
	 * ------------------------------------------------------------
	 */

	function __addHeaderLabel(){
		$('[data-event=coupon_header_LP]').attr({'data-label':location.href});
		$('[data-event=coupon_header_search]').attr({'data-label':location.href});
	}
	

	/**
	 * ------------------------------------------------------------
	 * ヘッダ残り時間
	 * ------------------------------------------------------------
	 */
	function __showHeaderTime(){
		var _$banner = $('[data-coupon-banner="banner"]');
		var _$reminderCounter = _$banner.find('[data-coupon-banner="reminder-counter"]');
		var _$reminderLabel = _$banner.find('[data-coupon-banner="reminder-label"]');
		var _$counterNumber = _$banner.find('[data-coupon-banner="counter-number"]');
		var _$counterUnit = _$banner.find('[data-coupon-banner="counter-unit"]');
		var _$reminderClosing = _$banner.find('[data-coupon-banner="reminder-closing"]');

		function _init(){
			if(_$banner.length){
				_createTime();
			}
		}

		/* リマインダーを表示 -------------------- */

		function _createTime(){
			var timeDay = 1000 * 60 * 60 * 24;
			var timeHour = 1000 * 60 * 60;
			var timeMinute = 1000 * 60;
			var timeClosing = 1000 * 60 * 10;
			var currentDate = new Date(window.__app.date);
			var currentDateTime = currentDate.getTime();
			var endDate = new Date(__couponData.enddt);
			var endDateTime = endDate.getTime();
			var remainingTime = endDateTime - currentDateTime;

			showReminder();

			/* リマインダーを表示 -------------------- */
			function showReminder() {
				var remainingTimeFractions;

				_$reminderLabel
					.text('本日のクーポン');

				if (remainingTime <= timeClosing) {
					_$reminderClosing
						.addClass('active');
				}
				else {
					remainingTimeFractions = remainingTime % timeMinute + 1000;

					updateCounter();

					_$reminderCounter
						.addClass('active');

					setTimeout(function() {
						updateReminder(remainingTimeFractions);
					}, remainingTimeFractions);
				}
			}

			/* カウントを更新 -------------------- */
			function updateCounter() {
				var number;
				var unit;

				if (remainingTime >= timeHour) {
					number = parseInt(remainingTime / timeHour, 10);
					unit = '時間';
				}
				else {
					number = parseInt(remainingTime / timeMinute, 10);
					unit = '分';
				}

				_$counterNumber
					.text(number);
				_$counterUnit
					.text(unit);
			}

			/* リマインダーを更新 -------------------- */
			function updateReminder(delay) {
				remainingTime -= delay;

				if (remainingTime <= timeClosing) {
					switchReminders();
				}
				else {
					updateCounter();

					setTimeout(function() {
						updateReminder(timeMinute);
					}, timeMinute);
				}
			}

			/* リマインダーを切り替え -------------------- */
			function switchReminders() {
				_$reminderCounter
					.removeClass('active');
				_$reminderClosing
					.addClass('active');
			}
		}
		_init();
	}


	/**
	 * ------------------------------------------------------------
	 * 開催中クーポンモーダル
	 * ------------------------------------------------------------
	 */
	function __showCouponModal(){
		function _init(){
			_bind();
		}

		function _bind(){
			$('[data-toggle="modalbox"]').filter('[data-target="#couponModal"]').one('click',function(){
				_getData();
			});
		}

		function _getData(){
			$.ajax({
				url:'/_cart/',
				dataType:'json',
				data:{
					c:'GetShopCouponList',
					ExclusionShopIds:__coupon.ExclusionShop
				}
			}).done(function(data){
				if(typeof data.item.error=='undefined'){
					_createElm(data);
				}else{
					_noCouponElm();
				}
			});
		}

		function _createElm(data){
			
			if(data.item.data.length){
				var tmpl=[
					'<li class="indexItem oneShopCoupon">',
						'<a class="feedLink coupon_c${weekday}_${highandlow} " href="${shopurl}" data-tracking="datalayer" data-action="click" data-event="coupon_modal" data-label="{{if favorite == 1}}favorite{{/if}}shop_${decodeURIComponent(shopname)}">',
							'<figure class="feedIcon">',
								'<div class="feedImgWrap"><img class="feedImg" src="${shopimg}" alt="${decodeURIComponent(shopname)}"></div>',
							'</figure>',
							'<div class="feedContent couponOutline">',
								'<div class="conponPrice">',
									'<span class="conponPriceAmount">',
										'&yen;${$.numFormat(amount)}',
									'</span>',
								'</div>',
							'</div>',
						'</a>',
					'</li>'
				].join('');

				if(data.item.data.length>8){
					$('#modalCouponBox').find('.andmore').removeClass('hide');
				}

				$.tmpl(tmpl,data.item.data.slice(0,8)).appendTo('#modalCouponList');

				$('#modalCouponShopCnt').html('<span>('+data.item.allshopcount+'ショップ）</span>');
			}else{
				_noCouponElm();

			}
		}

		function _noCouponElm(){
			var tmpl=[
				'<div class="noItem">',
					'<div class="icon"><img class="imgFluid" src="'+window.__app.storageOriginalUrl+'pc/coupon/icon_coupon_disabled.png"></div>',
					'<p class="txt m-t-lg">ご利用できるクーポンはありません</p>',
				'</div>',
			].join('');
			$('#modalCouponBox').replaceWith($.tmpl(tmpl));
		}



		_init();
	}

	__init();

})(jQuery, window, document);