/*-------------------------------------
SITE : http: //zozo.jp/
CREDIT : DESIGN BY STARTTODAY CO.,LTD.
CREATE : 2014.11.26
MODIFIED : 2016.10.12
Var : 1.0
MEMO : 2016.10.05 マウスオーバーで画像切り替え
MEMO : 2016.03.02 ESCキーでオーバーレイ破棄
-------------------------------------*/

({
	/*-------------------------------------
		初期化
	-------------------------------------*/
	init:function(){
		var _this=this;

		$(function(){
			_this.giftOverray.init();
			_this.changeImg();
		});
	},

	/*-------------------------------------
		ギフトオーバーレイ
	--------------------------------------*/
	giftOverray:{
		conf:{
			overlayHeight:null
		},

		init:function(){
			var _this=this;
			$(function(){
				$("#giftOver a").click( function(e) {
					_this.openGiftLP();
					return false;
				});
				$(".giftOver a").click( function(e) {
					_this.openGiftLP();
					return false;
				});
				$(".giftOver map").click( function(e) {
					_this.openGiftLP();
					return false;
				});
				$("#Close_ov").click(function() {
					_this.closeGiftLP();
				});
				$("#clearBg_ov").click(function() {
					_this.closeGiftLP();
				});
				$("#letsGift_ov").find('a').click(function() {
					_this.closeGiftLP();
					return false;
				});
				// ESCキーでポップアップ破棄
				$(document).keyup(function(e){
					if(e.keyCode == 27){
						_this.closeGiftLP();
					}
				});
			});
		},

		openGiftLP:function(){
			var _this=this;
			var _conf=this.conf;
			if ($("#giftOverray").css("display") == 'none' ) {
				_conf.overlayHeight=$(document).scrollTop();
				$("#giftOverray").css("top",_conf.overlayHeight);
				if($.ua.isIE7){}else{
					$("#clearBg_ov").removeClass('hide');
				}
				$("#giftOverray").fadeIn();
				return false;
			}
		},

		closeGiftLP:function(){
			var _this=this;
			var _conf=this.conf;
			if ($("#giftOverray").css("display") == 'block' ) {
				$("#clearBg_ov").addClass('hide');
				$("#giftOverray").fadeOut();
				$('body').animate({scrollTop:_conf.overlayHeight},300);
			}
		}
	},

	changeImg:function(){
		$ov = $('#giftOverray');
		$ov.find('.sub').find('img').mouseenter(function(){					// 画像にマウスが乗ったら
			var srcS =$(this).attr('src').split("_s");
			var srcB = (srcS[0]+'_b'+srcS[1]);
			$(this).closest('ul').closest('li').find('.mainImg').find('img').attr({src:srcB});	// メイン画像に差し替える
		});
	}

}).init();
