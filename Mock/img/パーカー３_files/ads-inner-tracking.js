(function($, window, document, undefined) {
	__adsInnerTrackingSearchpv();
	__adsInnerTrackingGoodspv();
	__adsInnerTrackingGoodscartputclick();
	__adsInnerTrackingCv();



	/**
	 * ------------------------------------------------------------
	 * dataLayerに追加
	 * ------------------------------------------------------------
	 */
	function __pushToDataLayer(properties) {
		window.dataLayer.push(properties);
	}



	/**
	 * ------------------------------------------------------------
	 * 検索結果impressionタグ
	 * ------------------------------------------------------------
	 */
	function __adsInnerTrackingSearchpv() {
		_init();



		/* ----------------------------------------
		 * 初期化
		 * ---------------------------------------- */

		function _init() {
			$(function() {
				if (window.__adsInnerSearchpv === undefined) {
					return false;
				}

				__pushToDataLayer(_getDataLayerProperties());
			});
		}



		/* ----------------------------------------
		 * dataLayerのプロパティを取得
		 * ---------------------------------------- */

		function _getDataLayerProperties() {
			var adsInnerSearchpv = window.__adsInnerSearchpv;

			return {
				event: 'zozoad_searchpv',
				zozoad_searchpv_queryid: adsInnerSearchpv.queryid,
				zozoad_searchpv_platform: adsInnerSearchpv.platform,
				zozoad_searchpv_query: adsInnerSearchpv.query,
				zozoad_searchpv_filtered: adsInnerSearchpv.filtered,
				zozoad_searchpv_totalhits: adsInnerSearchpv.totalhits,
				zozoad_searchpv_requests: adsInnerSearchpv.requests
			};
		}
	}



	/**
	 * ------------------------------------------------------------
	 * 商品詳細impressionタグ
	 * ------------------------------------------------------------
	 */
	function __adsInnerTrackingGoodspv() {
		_init();



		/* ----------------------------------------
		 * 初期化
		 * ---------------------------------------- */

		function _init() {
			$(function() {
				if (window.__adsInnerGoodspv === undefined) {
					return false;
				}

				__pushToDataLayer(_getDataLayerProperties());
			});
		}



		/* ----------------------------------------
		 * dataLayerのプロパティを取得
		 * ---------------------------------------- */

		function _getDataLayerProperties() {
			var adsInnerGoodspv = window.__adsInnerGoodspv;

			return {
				event: 'zozoad_goodspv',
				zozoad_goodspv_platform: adsInnerGoodspv.platform,
				zozoad_goodspv_item: adsInnerGoodspv.item
			};
		}
	}



	/**
	 * ------------------------------------------------------------
	 * カート投入タグ
	 * ------------------------------------------------------------
	 */
	function __adsInnerTrackingGoodscartputclick() {
		var _eventNamespace = '.__adsInnerTrackingGoodscartputclick';
		var _formSelector = '[data-ads-inner-tracking="goodscartputclick"]';

		_init();



		/* ----------------------------------------
		 * 初期化
		 * ---------------------------------------- */

		function _init() {
			$(function() {
				if (window.shopCommonConf === undefined) {
					return false;
				}

				if (window.shopCommonConf.shid === '802') {
					return false;
				}

				if ($(_formSelector).length === 0) {
					return false;
				}

				_buildElements();
			});
		}



		/* ----------------------------------------
		 * 要素を構築
		 * ---------------------------------------- */

		function _buildElements() {
			var $form = $(_formSelector);

			$form
				.on('submit' + _eventNamespace, function() {
					_sendData();
				});
		}



		/* ----------------------------------------
		 * データを送信
		 * ---------------------------------------- */

		function _sendData() {
			if (typeof ss_cart_in_log !== 'function') {
				return false;
			}

			var shopCommonConf = window.shopCommonConf;

			ss_cart_in_log(shopCommonConf.gdid, shopCommonConf.gid, shopCommonConf.tcaid, shopCommonConf.shid);
		}
	}



	/**
	 * ------------------------------------------------------------
	 * 購入完了タグ
	 * ------------------------------------------------------------
	 */
	function __adsInnerTrackingCv() {
		_init();



		/* ----------------------------------------
		 * 初期化
		 * ---------------------------------------- */

		function _init() {
			$(function() {
				if (window.__adsInnerCv === undefined) {
					return false;
				}

				__pushToDataLayer(_getDataLayerProperties());
			});
		}



		/* ----------------------------------------
		 * dataLayerのプロパティを取得
		 * ---------------------------------------- */

		function _getDataLayerProperties() {
			var adsInnerCv = window.__adsInnerCv;

			adsInnerCv.analytics.serviceName = 'zozo';

			return {
				event: 'zozoad_cv',
				zozoad_cv_analytics: adsInnerCv.analytics,
				zozoad_cv_items: adsInnerCv.items
			};
		}
	}
})(jQuery, window, document);
